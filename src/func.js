const getSum = (str1, str2) => {
  if(typeof str1 != "string" || typeof str2 != "string"){return false}
  let bf = false;
  let str = (str1 + str2).split("");
  for (let mem of str){
    if(isNaN(Number(mem))){
      bf = true;
      break;
    }
  }
  if(bf){return false}
  return (Number(str1) + Number(str2)).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  
  let ans = [""];
  let post = 0;
  let com = 0;
  for (let mem of listOfPosts){
    if (mem.author == authorName){
      post++;
    }

    if (mem.comments != undefined){
      for(let _com of mem.comments){
        if(_com.author == authorName){
          com++;
        }
      }      
    }
  }
  ans += `Post:${post},comments:${com}`

  return ans;
};

const tickets=(people)=> {
  if(people[0] != 25){return 'NO'}
    let d25 = 1;
    let d50 = 0;
  for(let i = 1;i < people.length;i++){
    switch(people[i]){
      case 25: d25++;
        break;
      case 50: d50++;
        break;
    }
    people[i] -= 25;

    if(people[i] >= 50){
      if(d50 > 0){people[i] -= 50;d50--} }
    if(people[i] >= 25){
      if(d25 > 0){people[i] -= 25;d25--} }

    if(people[i] != 0){
      return 'NO';
    }   
  }
  return 'YES';

};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
